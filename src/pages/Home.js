import { Card } from "antd"
import { useSelector } from "react-redux"



export function Home() {
    //Ici, on utilise le useSelector pour récupérer la valeur du user depuis le store
    const user = useSelector(state => state.auth.user);


    // useEffect(() => {
    //     AuthService.fetchAccount().then(data => console.log(data));
    // }, [])

    return (
        <Card>
            Home Page
            {user && <p>{user.email}</p>}
        </Card>
    )
}