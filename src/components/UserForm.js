
import { Form, Input, Button } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { register } from '../stores/auth-slice';


/**
 * 
 * Rien de particulier dans ce formulaire, juste on y utilise les formulaires de
 * Ant Design, mais tout ce qu'on fait là marcherait également avec n'importe quel 
 * gestion de form, l'idée est juste d'avoir un formulaire avec les champs nécessaire pour
 * un User
 */
export function UserForm() {

    const dispatch = useDispatch();
    const feedback = useSelector(state => state.auth.registerFeedback);

    const onFinish = (values) => {
        dispatch(register(values));
    }

    return (
        <Form
            name="basic"
            labelCol={{
                span: 8,
            }}
            wrapperCol={{
                span: 10,
            }}
            onFinish={onFinish}
        >
            {feedback && <p>{feedback}</p>}
            <Form.Item
                label="Email"
                name="email"
                rules={[
                    {
                        required: true,
                        message: 'Email is required',
                    },
                    {
                        type: 'email',
                        message: 'Please enter a valid email',
                    },
                ]}
            >
                <Input type="email" />
            </Form.Item>

            <Form.Item
                label="Password"
                name="password"
                rules={[
                    {
                        required: true,
                        message: 'Password is required',
                    },
                ]}
            >
                <Input.Password />
            </Form.Item>



            <Form.Item
                wrapperCol={{
                    offset: 8,
                    span: 16,
                }}
            >
                <Button type="primary" htmlType="submit">
                    Register
                </Button>
            </Form.Item>
        </Form>
    )
}