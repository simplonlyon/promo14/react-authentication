import { Table } from "antd";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchUsers } from "../stores/user-slice";

const columns = [
    {
        title: 'Id',
        dataIndex: 'id',
        key: 'id',
    },
    {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
    },
    {
        title: 'Role',
        dataIndex: 'role',
        key: 'role',
    }
];
export function ListUser() {
    const dispatch = useDispatch()
    const users = useSelector(state => state.users.list);

    useEffect(() => {
        dispatch(fetchUsers());
    }, [dispatch]);

    return (
        <Table dataSource={users} columns={columns} rowKey="id" />
    );
}